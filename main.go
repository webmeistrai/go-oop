package main

import "fmt"

func main() {

	// gen:=Generalinis{
	// 	lektuvas:true,
	// 		Direktorius:Direktorius{
	// 			 automobilis:true,
	// 			 Darbuotojas:dar,
	// 				},
	// }

	//  var zmogus [5]DarbuotojasFace

	// zmogus[0] = dar;

	list := new(DarbuotojasList)
	kitas := new(Darbuotojas)

	list.Add(kitas)
	dir := new(Direktorius)
	dir.SetVardas("Agnė")
	dir.SetPavarde("Petrauskaitė")
	list.Add(dir)
	fmt.Println("prieš:" + dir.Vardas())
	fmt.Println("Sąraše: " + list.First().Vardas())
	dar := list.First()
	dar.SetPavarde("Paulavičienė")

	dir.Darbuotojas.Naujas = "asdasd"
	fmt.Println("po: " + dir.Vardas())

	//  fmt.Println(dir.Vardas())

}
