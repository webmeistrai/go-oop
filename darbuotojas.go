package main

import "fmt"

type Darbuotojas struct {
	vardas  string
	pavarde string
	Naujas  string
}

type DarbuotojasFace interface {
	Vardas() string
	SetVardas(string)
	Pavarde() string
	SetPavarde(string)
}

func (this Darbuotojas) Vardas() string {
	fmt.Println(this.vardas)
	return this.vardas + " " + this.Pavarde()
}
func (this *Darbuotojas) SetVardas(s string) {

	this.vardas = s

}

func (this Darbuotojas) Pavarde() string {
	return this.pavarde
}
func (this *Darbuotojas) SetPavarde(s string) {
	this.pavarde = s
}

type DarbuotojasList struct {
	darbuotojai [10]DarbuotojasFace
}

func (this DarbuotojasList) First() DarbuotojasFace {
	return this.darbuotojai[0]
}
func (this *DarbuotojasList) Add(value DarbuotojasFace) {
	this.darbuotojai[0] = value
}
